import dfs
import view
import controller

from search_dataclasses import state


def main():

    v = view.TerminalView()
    v.show_welcome_msg()

    start_state = state.State([[3, 6, 4], [2, 5, 8], [7, 1, None]])
    finish_state = state.State([[None, 1, 2], [3, 4, 5], [6, 7, 8]])

    #  выше спросили пользователя, теперь через if надо сделать выбор модели
    mode = int(input('mode:'))
    if mode == 0:
        m = dfs.DepthFirstSearch(start_state, finish_state)
    elif mode == 1:
        depth = int(input('depth:'))
        m = dfs.DepthFirstSearch(start_state, finish_state, depth_lim=depth)
    else:
        raise RuntimeError("something")

    c = controller.BlindSearchController(view=v, model=m)
    c.start()
    c.pause()


if __name__ == "__main__":
    main()
