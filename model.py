from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List

from search_dataclasses.state import State
from search_dataclasses.vertex import Vertex


class BlindSearchModel(ABC):

    @abstractmethod
    def complete_search(self):
        '''Запустить автопоиск'''
        raise NotImplementedError

    @abstractmethod
    def is_search_ended(self):
        '''Закончен ли поиск (не нашли ничего по всем узлам/нашли)'''

    # проверка на нахождение нужного состояния
    @abstractmethod
    def goal_test(self) -> bool:
        '''Проверка текущего состояния на достижение цели'''
        raise NotImplementedError

    @abstractmethod
    def step_over(self):  # совершить один шаг алгоритма
        raise NotImplementedError

    @abstractmethod
    def get_last_added_vertexes(self) -> List[Vertex]:
        '''Возвращает вершины, добавленные на последнем шаге.
        Соответственно, надо их где-то хранить или
        придумать способ брать их из пройденных
        '''
        raise NotImplementedError

    @abstractmethod
    def get_last_repeated_vertexes(self) -> List[Vertex]:
        '''Возвращает вершины, состояния в которых были отброшены'''
        raise NotImplementedError

    @abstractmethod
    def get_fringe_state(self) -> List[Vertex]:
        '''Возвращает очередь из вершин в очереди,
        нужно для вывода информации по методе,
        интерпретироваться и обрабатываться эта очередь будет потом
        '''
        raise NotImplementedError

    @abstractmethod
    def get_current_vertex(self) -> Vertex:
        '''Возвращает вершину, которую алгоритм раскрыл сейчас'''
        raise NotImplementedError

    @abstractmethod
    def get_algo_name(self) -> str:
        '''Возвращает строку с названием алгоритма'''
        raise NotImplementedError

    @abstractmethod
    def get_start_state(self) -> State:
        '''Возвращает стартовое состояние'''
        raise NotImplementedError

    @abstractmethod
    def get_goal_state(self) -> State:
        '''Возвращает конечное состояние'''
        raise NotImplementedError

    @abstractmethod
    def get_depth_limit(self) -> int:
        '''Возвращает лимит глубины'''
        raise NotImplementedError

    class SetupInfo:
        def __init__(self, search: BlindSearchModel):
            self.algo_name = search.get_algo_name()
            self.start_state = search.get_start_state()
            self.goal_state = search.get_goal_state()
            self.depth_limit = search.get_depth_limit()

    def get_setup_info(self):
        '''Возвращает ```SetupInfo``` для текущего класса поиска'''

        return self.SetupInfo(self)

    class IterationInfo:
        def __init__(self, search: BlindSearchModel):
            self.added_vertexes = search.get_last_added_vertexes()
            self.repeated_vertexes = search.get_last_repeated_vertexes()
            self.fringe_state = search.get_fringe_state()
            self.current_vertex = search.get_current_vertex()
            self.is_goal_achieved = search.goal_test()

    def get_iteration_info(self):
        '''Возвращает ```IterationInfo``` для текущего класса поиска
        для текущей итерации
        '''
        return self.IterationInfo(self)

    @abstractmethod
    def get_checked_vertexes_count(self) -> int:
        '''Возвращает количество проверенных вершин'''
        raise NotImplementedError

    @abstractmethod
    def get_depth(self) -> int:
        '''Возвращает глубину'''
        raise NotImplementedError

    @abstractmethod
    def get_max_depth(self) -> int:
        '''Возвращает глубину'''
        raise NotImplementedError

    class CompletionInfo:
        '''Создать методы, прописать их по аналогии с IterationInfo'''
        def __init__(self, search: BlindSearchModel):
            self.checked_vertexes_count = search.get_checked_vertexes_count()
            self.depth = search.get_depth()
            self.max_depth = search.get_max_depth()
            self.goal_reached = search.goal_test()
            self.hash_size = search.get_hash_size()

    def get_hash_size():
        raise NotImplementedError

    def get_completion_info(self):
        '''Возвращает ```CompletionInfo``` для текущего класса поиска'''
        return self.CompletionInfo(self)
