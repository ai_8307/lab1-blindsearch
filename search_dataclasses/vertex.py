from __future__ import annotations

from .state import State


class Vertex:

    def __init__(self, state: State, depth: int, parent: Vertex):
        self.parent = parent
        self.depth = depth
        self.state = state

    def __str__(self):
    	return f"{self.state}\nDepth: {self.depth}"

    def get_transitions(self):
        return [
            Vertex(item, self.depth + 1, self)
            for item in self.state.get_transitions()
        ]
