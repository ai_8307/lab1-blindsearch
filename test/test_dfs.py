import os
import sys
import unittest

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from search_dataclasses.state import State
from dfs import DepthFirstSearch


class TestDFSMethods(unittest.TestCase):

    def test_same_goal_and_target(self):

        start = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])
        finish = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])

        search = DepthFirstSearch(start, finish)
        self.assertFalse(search.is_search_ended())

        search.step_over()

        self.assertTrue(search.is_goal_achieved())
        self.assertTrue(search.is_search_ended())

    def test_stepping_over_goal(self):
        start = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])
        finish = State([[1, 2, 3], [4, 5, 6], [7, 8, None]])

        search = DepthFirstSearch(start, finish)

        search.step_over()
        self.assertRaises(
            DepthFirstSearch.SearchError, lambda: search.step_over()
        )

    def test_goal_find(self):

        start_state = [[3, 6, 4], [2, 5, 8], [7, 1, None]]
        target_state = [[3, 5, 6], [2, None, 4], [7, 1, 8]]

        start = State(start_state)
        finish = State(target_state)

        search = DepthFirstSearch(start, finish)
        self.assertFalse(search.is_search_ended())

        # TODO: step until find (constant number of times)
        search.complete_search()

        self.assertTrue(search.is_goal_achieved())
        self.assertTrue(search.is_search_ended())

    def test_correct_number_of_steps(self):

        start_state = [[3, 6, 4], [2, 5, 8], [7, 1, None]]
        target_state = [[3, 6, None], [2, 5, 4], [7, 1, 8]]

        start = State(start_state)
        finish = State(target_state)

        search = DepthFirstSearch(start, finish)

        # TODO: step until find (constant number of times)
        search.step_over()
        search.step_over()
        search.step_over()

        self.assertTrue(search.is_goal_achieved())
        self.assertTrue(search.is_search_ended())

    def test_limited_search(self):

        start_state = [[3, 6, 4], [2, 5, 8], [7, 1, None]]
        target_state = [[3, 6, 4], [2, 8, None], [7, 5, 1]]

        start = State(start_state)
        finish = State(target_state)

        search = DepthFirstSearch(start, finish, depth_lim=3)

        for _ in range(12):
            search.step_over()

        self.assertTrue(search.is_goal_achieved())
        self.assertTrue(search.is_search_ended())

    def test_limited_search_goal_not_find(self):

        start_state = [[3, 6, 4], [2, 5, 8], [7, 1, None]]
        target_state = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

        start = State(start_state)
        finish = State(target_state)

        search = DepthFirstSearch(start, finish, depth_lim=3)

        search.complete_search()

        self.assertFalse(search.is_goal_achieved())
        self.assertTrue(search.is_search_ended())


if __name__ == "__main__":
    unittest.main()
