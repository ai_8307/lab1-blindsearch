from typing import List
from model import BlindSearchModel
from search_dataclasses import state
from search_dataclasses import vertex
from collections import deque


class DepthFirstSearch(BlindSearchModel):

    class SearchError(RuntimeError):

        def __init__(self, *args: object) -> None:
            super().__init__(*args)

    def __init__(self,
                 start: state.State,
                 finish: state.State,
                 depth_lim=None) -> None:

        self.step_overs = 0
        self.max_depth = 0

        self.depth_lim = depth_lim
        self.start_state = start
        self.target_state = finish
        self.current_vertex = vertex.Vertex(
            self.start_state, depth=0, parent=None
        )

        self.goal_reached = False

        self.last_added_vertexes = None
        self.last_visited_vertexes = None

        self.fringe = deque()
        self.fringe.append(self.current_vertex)

        self.visited = set()

    def is_fringe_empty(self):
        return not self.fringe

    def is_search_ended(self):
        return self.goal_test() or self.is_fringe_empty()

    def complete_search(self):
        while not self.is_search_ended():
            self.step_over()

    def is_goal_achieved(self) -> bool:
        return self.current_vertex.state == self.target_state

    def goal_test(self) -> bool:
        return self.goal_reached

    def get_not_visited_vertexes(self, v: vertex.Vertex):
        reversed_transitions = v.get_transitions()[::-1]
        self.last_visited_vertexes = [
            i for i in reversed_transitions if i.state in self.visited
        ]
        return [i for i in reversed_transitions if i.state not in self.visited]

    def connect_to_current(self, vs: List[vertex.Vertex]):
        for v in vs:
            v.parent = self.current_vertex

    def step_over(self):
        if not self.is_search_ended():
            if self.max_depth < self.get_depth():
                self.max_depth = self.get_depth()

            self.step_overs += 1
            self.current_vertex = self.fringe.pop()
            if self.is_goal_achieved():
                self.finish_search()
            else:
                self.traverse_tree()
        else:
            raise self.SearchError("Search is already finished!")

    def traverse_tree(self):
        new_vertexes = self.get_not_visited_vertexes(self.current_vertex)
        new_vertexes_depth = self.current_vertex.depth + 1
        self.put_child_vertexes_in_fringe(new_vertexes, new_vertexes_depth)
        self.visited.add(self.current_vertex.state)

    def put_child_vertexes_in_fringe(self, vs: List[vertex.Vertex], vs_depth):
        if self.is_fringe_insertion_possible(vs_depth):
            self.last_added_vertexes = vs
            self.fringe.extend(vs)

    def is_fringe_insertion_possible(self, vertex_depth):
        if self.depth_lim is None:
            return True
        elif vertex_depth <= self.depth_lim:
            return True
        return False

    def finish_search(self):
        self.goal_reached = True

    def backtrack(self) -> List[vertex.Vertex]:
        track = []
        current = self.current_vertex
        while(current):
            track.append(current)
            current = current.parent

        return track

    def get_last_added_vertexes(self) -> List[vertex.Vertex]:
        return self.last_added_vertexes

    def get_last_repeated_vertexes(self) -> List[vertex.Vertex]:
        return self.last_visited_vertexes

    def get_fringe_state(self) -> deque:
        return self.fringe

    def get_current_vertex(self) -> vertex.Vertex:
        return self.current_vertex

    def get_goal_state(self) -> state.State:
        return self.target_state

    def get_depth_limit(self) -> int:
        return self.depth_lim

    def get_max_depth(self) -> int:
        return self.max_depth

    def get_algo_name(self) -> str:
        name = 'Поиск в глубину'
        if self.depth_lim:
            name += f' с ограничением {self.depth_lim}'
        return name

    def get_checked_vertexes_count(self) -> int:
        return self.step_overs

    def get_depth(self) -> int:
        return self.current_vertex.depth

    def get_start_state(self) -> state.State:
        return self.start_state

    def get_hash_size(self):
        return len(self.visited) + self.get_checked_vertexes_count()
